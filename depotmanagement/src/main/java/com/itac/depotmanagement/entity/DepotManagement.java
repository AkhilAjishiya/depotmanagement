package com.itac.depotmanagement.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "depot_management")
public class DepotManagement extends Auditable<String>{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long depotId;
	
	private long  organizationId;
	
	private String mechanic;
	
	private String despatcher;
	
	private String address;
	
	private Boolean isDeleted;

}
