package com.itac.depotmanagement.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "users_org")
public class UserOrganization extends Auditable<String>{

	@Id
	@GeneratedValue
	private long userId;
	
	private long organizationId;
}
