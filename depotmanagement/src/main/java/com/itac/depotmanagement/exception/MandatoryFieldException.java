package com.itac.depotmanagement.exception;

public class MandatoryFieldException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1026685789732986581L;
	
	public MandatoryFieldException(String message) {
		super(message);
	}

	
	
	

}
