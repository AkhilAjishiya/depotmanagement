package com.itac.depotmanagement.exception.handler;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.itac.depotmanagement.exception.MandatoryFieldException;

@ControllerAdvice
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler{
	
	@ExceptionHandler(MandatoryFieldException.class)
	public ResponseEntity<Object>getMandatoryFieldExcpetion(MandatoryFieldException ex, WebRequest request){
		List<Object>details = new ArrayList<Object>();
		details.add(ex.getMessage());
		details.add(LocalDateTime.now());
		return new ResponseEntity<Object>(details,HttpStatus.BAD_REQUEST);
		
	}

}
