package com.itac.depotmanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.itac.depotmanagement.entity.DepotManagement;

@Repository
public interface DepotManagementRepository extends JpaRepository<DepotManagement, Long>{

	

	@Query(value = "select d from DepotManagement d where d.organizationId = organizationId")
	List<DepotManagement> findAllDepotsByOrganization(long organizationId);

	

}
