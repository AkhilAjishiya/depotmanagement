package com.itac.depotmanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.itac.depotmanagement.entity.UserOrganization;

public interface UserOrganizationRepository extends JpaRepository<UserOrganization, Long>{

	@Query(value = "select a from UserOrganization  a where a.userId = userId")
	UserOrganization findByUserId(long userId);

}
