package com.itac.depotmanagement.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itac.depotmanagement.entity.DepotManagement;
import com.itac.depotmanagement.exception.MandatoryFieldException;
import com.itac.depotmanagement.service.DepotManagementService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class DepotManagementController {

	@GetMapping("/")
	public String get() {
		return "hello world";
	}

	@Autowired
	private DepotManagementService depotManagementService;

	@PostMapping("/create-depot")
	public ResponseEntity<DepotManagement> createDepot(@RequestBody DepotManagement depotManagement)
			throws MandatoryFieldException {
		return new ResponseEntity<DepotManagement>(depotManagementService.createDepot(depotManagement), HttpStatus.OK);

	}

	@PutMapping("/update-depot/{depotId}")
	public ResponseEntity<DepotManagement> updateDepot(@PathVariable("depotId") long depotId,
			@RequestBody DepotManagement depotManagement) throws MandatoryFieldException {
		return new ResponseEntity<DepotManagement>(depotManagementService.updateDepot(depotId, depotManagement),
				HttpStatus.OK);
	}

	@DeleteMapping("/delete-depot/{depotId}")
	public ResponseEntity<DepotManagement> deleteDepot(@PathVariable("depotId") long depotId)
			throws MandatoryFieldException {
		return new ResponseEntity<DepotManagement>(depotManagementService.deleteDepot(depotId), HttpStatus.OK);
	}

	@GetMapping("/list-depot/{userId}")
	public ResponseEntity<List<DepotManagement>> listDepots(@PathVariable("userId") long userId)
			throws MandatoryFieldException {
		return new ResponseEntity<List<DepotManagement>>(depotManagementService.listDepots(userId), HttpStatus.OK);

	}

}
