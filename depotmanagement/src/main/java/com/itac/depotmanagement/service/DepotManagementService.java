package com.itac.depotmanagement.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.itac.depotmanagement.entity.DepotManagement;
import com.itac.depotmanagement.exception.MandatoryFieldException;



public interface DepotManagementService {

	DepotManagement createDepot(DepotManagement depotManagement) throws MandatoryFieldException;

	DepotManagement updateDepot(long depotId, DepotManagement depotManagement) throws MandatoryFieldException;

	DepotManagement deleteDepot(long depotId) throws MandatoryFieldException;

	List<DepotManagement> listDepots(long userId) throws MandatoryFieldException;

	

}
