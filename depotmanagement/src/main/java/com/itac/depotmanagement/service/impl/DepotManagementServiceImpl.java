package com.itac.depotmanagement.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itac.depotmanagement.entity.DepotManagement;
import com.itac.depotmanagement.entity.UserOrganization;
import com.itac.depotmanagement.exception.MandatoryFieldException;
import com.itac.depotmanagement.repository.DepotManagementRepository;
import com.itac.depotmanagement.repository.UserOrganizationRepository;
import com.itac.depotmanagement.service.DepotManagementService;

@Service
public class DepotManagementServiceImpl implements DepotManagementService {

	@Autowired
	private DepotManagementRepository depotManagementRepository;
	
	@Autowired
	private UserOrganizationRepository UserOrganizationRepository;

	@Override
	public DepotManagement createDepot(DepotManagement depotManagement) throws MandatoryFieldException {
		if (depotManagement.getOrganizationId() == 0) {
			throw new MandatoryFieldException("please provide valid organization Id");
		}
		return depotManagementRepository.save(depotManagement);
	}

	@Override
	public DepotManagement updateDepot(long depotId, DepotManagement depotManagement) throws MandatoryFieldException {
		Optional<DepotManagement> depotEntity = depotManagementRepository.findById(depotId);
		if (depotEntity == null) {
			throw new MandatoryFieldException("please provide a valid depot id");
		}
		depotManagement.setDepotId(depotEntity.get().getDepotId());
		return depotManagementRepository.save(depotManagement);
	}

	@Override
	public DepotManagement deleteDepot(long depotId) throws MandatoryFieldException {
		Optional<DepotManagement> depotEntity = depotManagementRepository.findById(depotId);
		if (depotEntity == null) {
			throw new MandatoryFieldException("please provide a valid depot id");
		}
		depotManagementRepository.deleteById(depotId);
		return null;
	}

	@Override
	public List<DepotManagement> listDepots(long userId) throws MandatoryFieldException {
		if (userId == 0) {
			throw new MandatoryFieldException("please provide a valid user Id");
		}
		UserOrganization entity = UserOrganizationRepository.findByUserId(userId);
		
		List<DepotManagement> list = depotManagementRepository.findAllDepotsByOrganization(entity.getOrganizationId());
		return list;
	}

}
