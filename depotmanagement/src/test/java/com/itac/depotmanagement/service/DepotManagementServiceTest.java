package com.itac.depotmanagement.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.itac.depotmanagement.entity.DepotManagement;
import com.itac.depotmanagement.entity.UserOrganization;
import com.itac.depotmanagement.exception.MandatoryFieldException;
import com.itac.depotmanagement.repository.DepotManagementRepository;
import com.itac.depotmanagement.repository.UserOrganizationRepository;
import com.itac.depotmanagement.service.DepotManagementService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class DepotManagementServiceTest {

	@Mock
	private DepotManagementService depotManagementService;

	@Mock
	private DepotManagementRepository depotManagementRepository;

	@Mock
	private UserOrganizationRepository userOrganizationRepository;

	@BeforeEach
	public void init() throws MandatoryFieldException {
		DepotManagement depotEntity = new DepotManagement();
		depotEntity.setDepotId(1L);
		depotEntity.setOrganizationId(1L);
		depotManagementService.createDepot(depotEntity);
	}

	@Test
	public void createDepot() throws MandatoryFieldException {
		DepotManagement depotEntity = new DepotManagement();
		depotEntity.setDepotId(2L);
		depotEntity.setOrganizationId(1L);
		when(depotManagementService.createDepot(depotEntity)).thenReturn(depotEntity);

	}

	@Test
	public void updateDepot() throws MandatoryFieldException {
		long depotId = 1L;
		DepotManagement depotEntity = new DepotManagement();
		depotEntity.setOrganizationId(2L);

		when(depotManagementService.updateDepot(depotId, depotEntity)).thenReturn(depotEntity);

	}

	@Test
	public void deleteDepot() throws MandatoryFieldException {

		when(depotManagementService.deleteDepot(2)).thenReturn(null);
		assertEquals(null, depotManagementService.deleteDepot(2));

	}

	@Test
	public void listDepots() throws MandatoryFieldException {

		
		DepotManagement dm = new DepotManagement();
		dm.setDepotId(2L);
		dm.setOrganizationId(1L);
		dm.setMechanic("marsh");
		when(depotManagementRepository.findAll()).thenReturn(Arrays.asList(dm));
		when(depotManagementRepository.save(dm)).thenReturn(dm);
		
		  UserOrganization user = new UserOrganization(); user.setUserId(1L);
		  user.setOrganizationId(1L);
		  when(userOrganizationRepository.save(user)).thenReturn(user);
		  
		  //user = userOrganizationRepository.findByUserId(1L);
		  
		  List<DepotManagement>list =
		  depotManagementService.listDepots(user.getUserId()); //List<DepotManagement>
		 // testList = depotManagementRepository //.findAllDepotsByOrganization(1);
		  
		  assertThat(list).hasSize(0);
		 

	}

}
